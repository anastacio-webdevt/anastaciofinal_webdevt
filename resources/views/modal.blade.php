<!-- Add Modal -->
<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Add New Food</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'save']) !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Name', 'Name') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Name', '', ['class' => 'form-control', 'placeholder' => 'Input Name', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Location', 'Location') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Location', '', ['class' => 'form-control', 'placeholder' => 'Input Location', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Carbohydrates', 'Carbohydrates') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Carbohydrates', '', ['class' => 'form-control', 'placeholder' => 'Input Carbohydrates', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Protein', 'Protein') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Protein', '', ['class' => 'form-control', 'placeholder' => 'Input Protein', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Fats', 'Fats') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Fats', '', ['class' => 'form-control', 'placeholder' => 'Input Fats', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
