@extends('app')

@section('content')
    <h1 class="page-header text-center">Food List</h1>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h2>Food Table
                <button type="button" data-target="#addnew" data-toggle="modal" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Food</button>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table class="table table-bordered table-responsive table-striped">
                <thead>
                <th>Name</th>
                <th>Location</th>
                <th>Carbohydrates</th>
                <th>Protein</th>
                <th>Fats</th>
                <th>Action</th>
                </thead>
                <tbody>
                @foreach($Food as $food)
                    <tr>
                        <td>{{$food->Name}}</td>
                        <td>{{$food->Location}}</td>
                        <td>{{$food->Carbohydrates}}</td>
                        <td>{{$food->Protein}}</td>
                        <td>{{$food->Fats}}</td>
                        <td><a href="#edit{{$food->id}}" data-toggle="modal" class="btn btn-success"><i class='fa fa-edit'></i> Edit</a> <a href="#delete{{$food->id}}" data-toggle="modal" class="btn btn-danger"><i class='fa fa-trash'></i> Delete</a>
                            @include('action')
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
