<!-- Edit Modal -->
<div class="modal fade" id="edit{{$member->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Edit Food</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($Food, [ 'method' => 'patch','route' => ['member.update', $food->id] ]) !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Name', 'Name') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Name', $food->Name, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Location', 'Location') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Location', $food->Location, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Carbohydrates', 'Carbohydrates') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Carbohydrates', $food->Carbohydrates, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Protein', 'Protein') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Protein', $member->Protein, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:7px;">
                            {!! Form::label('Fats', 'Fats') !!}
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('Fats', $food->Fats, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                {{Form::button('<i class="fa fa-check-square-o"></i> Update', ['class' => 'btn btn-success', 'type' => 'submit'])}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="delete{{$food->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Delete Member</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($Food, [ 'method' => 'delete','route' => ['food.delete', $food->id] ]) !!}
                <h4 class="text-center">Are you sure you want to delete Member?</h4>
                <h5 class="text-center">Name: {{$food->Name}} {{$food->Location}} {{$food->Carbohydrates}} {{$food->Protein}} {{$food->Fats}}</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                {{Form::button('<i class="fa fa-trash"></i> Delete', ['class' => 'btn btn-danger', 'type' => 'submit'])}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
