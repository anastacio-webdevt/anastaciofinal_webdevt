let id = $("input[name*='book_id']")
id.attr("readonly","readonly");


$(".btnedit").click( e =>{
    let textvalues = displayData(e);

    ;
    let foodname = $("input[name*='Name']");
    let foodlocation = $("input[name*='Location']");
    let foodcarbs = $("input[name*='Carbohydrates']");
    let foodprotein = $("input[name*='Protein']");
    let foodfats = $("input[name*='Fats']");

    id.val(textvalues[0]);
    foodname.val(textvalues[1]);
    foodlocation.val(textvalues[2]);
    foodcarbs.val(textvalues[3]);
    foodprotein.val(textvalues[4]);
    foodfats.val(textvalues[5]);
});


function displayData(e) {
    let id = 0;
    const td = $("#tbody tr td");
    let textvalues = [];

    for (const value of td){
        if(value.dataset.id == e.target.dataset.id){
            textvalues[id++] = value.textContent;
        }
    }
    return textvalues;

}
