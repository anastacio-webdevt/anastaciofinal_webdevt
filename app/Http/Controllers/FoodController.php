<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function index(){
        return view('show');
    }

    public function getFood(){
        $food = Food::all();

        return view('show')->with('food', $food);
    }

    public function save(Request $request){
        $food = new food;
        $food->Name = $request->input('Name');
        $food->Location = $request->input('Location');
        $food->Carbohydrates = $request->input('Carbohydrates');
        $food->Protein = $request->input('Protein');
        $food->Fats = $request->input('Fats');
        $food->save();

        return redirect('/');
    }

    public function update(Request $request, $id){
        $food = Food::find($id);
        $input = $request->all();
        $food->fill($input)->save();

        return redirect('/');
    }

    public function delete($id)
    {
        $food = Food::find($id);
        $food->delete();

        return redirect('/');
    }
}
